package com.example.ilearnenglish.game.ui

import android.widget.GridLayout
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Preview
@Composable
fun preview(){
    GameTab()
}
@Composable
fun GameTab(modifier: Modifier= Modifier){
    Column(modifier) {
        Text(text = "Selecciona los pares")
        Row() {
            Tabs(Modifier.weight(1f),list = listOf("",""))
            Tabs(Modifier.weight(1f),list = listOf("",""))
        }
    }
}

@Composable
fun Tabs(modifier: Modifier= Modifier,list:List<String>){
    LazyColumn(modifier) {
        items(list.size) { photo ->
            TabWidget(text="fdfdf")
        }
    }
}

@Composable
fun TabWidget(modifier: Modifier= Modifier, text:String){
    val borderStroke = Color.Blue
    Box(
        modifier
            .fillMaxWidth()
            .padding(10.dp)
            .height(40.dp)
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 400,
                    easing = LinearOutSlowInEasing
                )
            )
            .clip(RoundedCornerShape(10.dp))
            .clickable {

            }
            .border(
                width = 1.dp,
                color = borderStroke,
                shape = RoundedCornerShape(10.dp)
            )
            .background(Color.LightGray),
        contentAlignment= Alignment.Center
        ) {

        Text(text = text)
    }
}
@Composable
fun BubbleTap(text:String, icon:Int, back: Color,
              tap:Boolean,
              tapReturn:(Boolean)->Unit
){
    Row(
        modifier = Modifier
            .height(40.dp)
            .animateContentSize(
                animationSpec = tween(
                    durationMillis = 400,
                    easing = LinearOutSlowInEasing
                )
            )
            .clip(RoundedCornerShape(10.dp))
            .clickable {
                tapReturn(!tap)
            }
            .background(color = back)
            .padding(horizontal = 10.dp, vertical = 5.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painterResource(id = icon) ,"", modifier = Modifier
                .height(20.dp)
                .width(20.dp),
            tint = Color.White
        )
        Spacer(modifier = Modifier.width(5.dp))
        Text(
            text = text,textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            color = Color.White,
            fontSize = 15.sp,
        )
    }
}